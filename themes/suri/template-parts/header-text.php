<?php
/**
 * The template part for displaying header text
 *
 * Display site title and site description.
 *
 * @package Suri
 * @since 0.0.6
 */

?>

<div<?php suri_attr( 'title-area' )?>><a href="/"></a></div><!-- .title-area -->
